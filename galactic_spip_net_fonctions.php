<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS['puce'] = '- ';

// pour les forums
function raccourcir_nom($nom) {
	if (strpos($nom, "@")) {
		$nom = substr($nom, 0, strpos($nom, "@"));
	}
	return $nom;
}

// pour afficher proprement le nom des langues
function afficher_nom_langue ($lang) {
	if (preg_match(",^oc(_|$),", $lang))
		return "occitan";
	else
		return traduire_nom_langue($lang);
}

// Prend une URL et lui ajoute/retire une ancre après l'avoir nettoyee
// pour l'ancre on vire les non alphanum du debut, et on remplace ceux dans le mot par -
// https://code.spip.net/@ancre_url replace{}
function ancre_url_propre($url, $ancre) {
	// lever l'#ancre
	if (preg_match(',^([^#]*)(#.*)$,', $url, $r)) {
		$url = $r[1];
	}
	$ancre = preg_replace(array('/^[^-_a-zA-Z0-9]+/', '/[^-_a-zA-Z0-9]/'), array('', '-'), $ancre);
	return $url .'#'. $ancre;
}

