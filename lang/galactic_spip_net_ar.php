<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_spip_net?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'البحث بين الموقعين',

	// D
	'derniers_sites_realises_avec_spip' => 'أحدث المواقع المصممة بواسطة سبيب',

	// G
	'glossaire' => 'الفهرس',

	// L
	'liens_utiles' => 'روابط مفيدة',

	// M
	'maj' => 'تم التحديث في',

	// T
	'trad_bilan' => 'جردة الترجمات',
	'trad_espace' => 'مساحة الترجمة',

	// W
	'web_independant' => 'من أجل نسيج مستقل',
	'web_independant_manifeste' => 'بيان من أجل نسيج مستقل'
);
