<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_spip_net?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Chercher parmi les signataires',

	// D
	'deprecie' => 'Dépréciés depuis : @version@',
	'derniers_sites_realises_avec_spip' => 'Derniers sites réalisés avec SPIP',

	// G
	'glossaire' => 'Glossaire',

	// L
	'liens_utiles' => 'Liens utiles',

	// M
	'maj' => 'mis à jour le ',

	// N
	'nouveau' => 'Apparu en : @version@',

	// P
	'pas_de_resultat_pour_la_recherche' => 'Pas de résultat pour la recherche.',

	// S
	'supprime' => 'Supprimé depuis : @version@',

	// T
	'trad_bilan' => 'Bilan des traductions',
	'trad_espace' => 'Espace de traduction',

	// W
	'web_independant' => 'Pour un web indépendant',
	'web_independant_manifeste' => 'Manifeste pour un web indépendant'
);
