<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_spip_net?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'Zoek in de ondertekenaars',

	// D
	'derniers_sites_realises_avec_spip' => 'Nieuwste met SPIP gerealiseerde sites',

	// G
	'glossaire' => 'Woordenlijst',

	// L
	'liens_utiles' => 'Nuttige koppelingen',

	// M
	'maj' => 'aangepast op ',

	// P
	'pas_de_resultat_pour_la_recherche' => 'Geen zoekresultaat.',

	// T
	'trad_bilan' => 'Balans van vertalingen',
	'trad_espace' => 'Vertaalruimte',

	// W
	'web_independant' => 'Voor een onafhankelijk web',
	'web_independant_manifeste' => 'Handvest voor een onafhankelijk web'
);
