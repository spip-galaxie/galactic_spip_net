<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_spip_net?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chercher_parmi_les_signataires' => 'جستجو در ميان امضاء كنندگان',

	// D
	'derniers_sites_realises_avec_spip' => 'اخرين سايت‌هاي ساخته شده با اسپيپ ',

	// G
	'glossaire' => 'واژه‌ نامه ',

	// L
	'liens_utiles' => 'پيوندهاي مفيد ',

	// M
	'maj' => 'روزآمد سازي در ',

	// T
	'trad_bilan' => 'بيلان ترجمه‌ها',
	'trad_espace' => 'جايگاه ترجمه ',

	// W
	'web_independant' => 'براي يك وب مستقل ',
	'web_independant_manifeste' => 'مانيفست وب مستقل '
);
