<?php
/**
 * Fichier d'options de spip.net
 * 
 * Penser à activer les URLs 'trad'.
 * et à intégrer le htaccess.txt dans le .htaccess ou le vhost.
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

define('_SPIP_TOPNAV', true);
define('_BOUSSOLE_ALIAS_SERVEUR', 'spip');

// pas d'autobr dans l'aide, ca casse des images (autobr a debug donc)
if (_request('lang_aide')) {
	define('_AUTOBR','');
}

// antispam de signature de forum (a integrer dans akismet ?)
function inc_controler_signature($id_article, $nom_email, $adresse_email, $message, $nom_site, $url_site, $url_page) {
	if (
		$a = @unserialize($GLOBALS['meta']['spampetitions'])  
		AND strlen($a = $a['regexp'])
		AND (
			preg_match($a, $nom_email)
			OR preg_match($a, $adresse_email)
			OR preg_match($a, $message)
		)
	) {
		spip_log("spam detecte sur la petition $id_article", 'spam');
		return false; // spam detecte
	}

	return inc_controler_signature_dist($id_article, $nom_email, $adresse_email, $message, $nom_site, $url_site, $url_page);
}

function suivi_versions_spip_net() {
	return [
		'nouveau' => 14,
		'deprecie' => 17,
		'supprime' => 18,
	];
}
