<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2016                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * chargement des valeurs par defaut des champs du `#FORMULAIRE_RECHERCHE_AJAX{#SELF}`
 *
 * on peut lui passer l'url de destination en premier argument
 * on peut passer une deuxième chaine qui va différencier le formulaire pour pouvoir en utiliser plusieurs sur une même page
 *
 * @param string $lien URL où amène le formulaire validé
 * @param string $placeholder Pour indiquer un titre spécifique au placeholder
 * @return array
 */
function formulaires_recherche_ajax_charger_dist($lien = '', $placeholder = '') {
	if ($GLOBALS['spip_lang'] != $GLOBALS['meta']['langue_site']) {
		$lang = $GLOBALS['spip_lang'];
	} else {
		$lang = '';
	}

	$class = 'ajax';
	$action = ($lien ? $lien : generer_url_public('recherche')); # action specifique, ne passe pas par Verifier, ni Traiter
	return
		array(
			'action' => $action,
			'recherche' => _request('recherche'),
			'lang' => $lang,
			'class' => $class,
			'placeholder' => $placeholder,
			'_id_champ' => $class ? substr(md5($action . $class), 0, 4) : 'recherche'
		);
}
