<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function liens_implicite_spip_dist($texte, $id, $type, $args, $ancre, $connect = '') {
	if (!$id = intval($id)) {
		return false;
	}

	static $liens_spip = array(
		1 => 1309,
		10 => 1309,
		103 => 1309,
		104 => 1309,
		105 => 1309,
		12 => 1310,
		121 => 1310,
		13 => 1253,
		14 => 1832,
		15 => 1911,
		16 => 1965,
		17 => 2102,
		171 => 2102,
		172 => 2102,
		18 => 2991,
		181 => 2991,
		182 => 3173,
		183 => 3333,
		19 => 3368,
		191 => 3462,
		192 => 3567,
		20 => 3784,
		21 => 4728,
		30 => 5427,
	);

	if (isset($liens_spip[$id])) {
		$id = $liens_spip[$id];
		$id_trad = sql_getfetsel('id_article', 'spip_articles',
			"id_trad=" . $id
			. " AND lang=" . sql_quote($GLOBALS['spip_lang'])
		);
		if ($id_trad) {
			$id = $id_trad;
		}
		return array('article', $id);
	}
	spip_log("raccourci spip$id inconnu");
	return false;
}
